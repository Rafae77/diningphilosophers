package philosopherProgramm;

import java.awt.*;

public class GraphicChopstick extends Paint {
  private Point coordinateStart;
  private Point coordinateEnd;
  private int angle;
  private int degreeValueMutiply = 72;
  private int degreeValueSum = 36;
  private int chopstickPositionYValueSum = 15;
  static private int chopstickId = 0;

  public GraphicChopstick(Point center, Point coordinateStart, Point coordinateEnd) {
    super.setInitialColor();
    this.angle = degreeValueMutiply * chopstickId + degreeValueSum;
    this.coordinateStart = new Point(rotation(coordinateStart.x, coordinateStart.y, center.x, center.y, angle));
    this.coordinateEnd = new Point(rotation(coordinateEnd.x, coordinateEnd.y, center.x, center.y, angle));
    fixChopTipPositionY(coordinateStart);
    fixChopTipPositionY(coordinateEnd);
    chopstickId++;
  }

  private void fixChopTipPositionY(Point chop) {
    chop.y += chopstickPositionYValueSum;
  }

  public void draw(Graphics g) {
    g.setColor(color);
    g.drawLine(coordinateStart.x, coordinateStart.y, coordinateEnd.x, coordinateEnd.y);
  }

  public Point rotation(int coordinateX, int coordinateY, int centerX, int centerY, int angle) {
    Point nouvellePosition = new Point();
    double valeurAngle = Math.PI / (180.0 / angle);

    nouvellePosition.x = (int) (coordinateX * Math.cos(valeurAngle) - coordinateY * Math.sin(valeurAngle)
        - Math.cos(valeurAngle) * centerX + Math.sin(valeurAngle) * centerY + centerX);
    nouvellePosition.y = (int) (coordinateX * Math.sin(valeurAngle) + coordinateY * Math.cos(valeurAngle)
        - Math.sin(valeurAngle) * centerX - Math.cos(valeurAngle) * centerY + centerY) + chopstickPositionYValueSum;
    return (nouvellePosition);
  }

  public Point getcoordinatesStart() {
    return this.coordinateStart;
  }

  public Point getcoordinatesEnd() {
    return this.coordinateEnd;
  }
}