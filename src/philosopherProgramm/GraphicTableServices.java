package philosopherProgramm;

import java.awt.Graphics;

public class GraphicTableServices extends GraphicTable {
  static private int enAttente = 0;
  static private int numberHungryPhilosophers = 0;
  protected Philosopher p0;
  protected Philosopher p1;
  protected Philosopher p2;
  protected Philosopher p3;
  protected Philosopher p4;

  public GraphicTableServices() {
    super();
  }
  
  public void createPhilosophers(){
    p0 = new Philosopher(0, this, 0, 4);
    p1 = new Philosopher(1, this, 1, 0);
    p2 = new Philosopher(2, this, 2, 1);
    p3 = new Philosopher(3, this, 3, 2);
    p4 = new Philosopher(4, this, 4, 3);
  }

  public void philopherStartDinning() {
    p0.start();
    p1.start();
    p2.start();
    p3.start();
    p4.start();
  }

  public synchronized void becomesHungry(int philosopherId) {
    while (numberHungryPhilosophers == NOMBRE_CHOPSTICKS_PHILOSOPHERS || enAttente > 0) {
      attente();
    }
    numberHungryPhilosophers++;
    plates[philosopherId].setColor(philosopherId);
    super.repaint();
  }

  private void attente() {
    try {
      enAttente++;
      wait();
      enAttente--;
    } catch (InterruptedException e) {
      System.out.println("Interruption dans l'attente");
      System.out.println(e);
    }
  }

  public synchronized void doThinking(int philosopherId) {
    plates[philosopherId].setInitialColor();
    super.repaint();
    numberHungryPhilosophers--;
    notify();
  }

  public synchronized void take(int chopstickId) {
    while (!chopsticksFlag[chopstickId]) {
      try {
        if (!chopsticksFlag[0] && !chopsticksFlag[1] && !chopsticksFlag[2] && !chopsticksFlag[3]
            && !chopsticksFlag[4]) {
          releaseChopstick(chopstickId);
          release(chopstickId);
        } else {
          wait();
        }
      } catch (InterruptedException e) {
        System.out.println("boom !");
      }
    }
    chopsticksFlag[chopstickId] = false;
  }

  public synchronized void release(int chopstickId) {
    chopsticksFlag[chopstickId] = true;
    notifyAll();
  }

  public void takeChopstick(int philosopherId, int chopstickId) {
    System.out.println(String.format("repainting philosopher %d - color %d ", philosopherId, chopstickId));
    chopsticks[chopstickId].setColor(philosopherId);
    super.repaint();
  }

  public void releaseChopstick(int chopstickId) {
    chopsticks[chopstickId].setInitialColor();
    super.repaint();
  }

  public void paint(Graphics graphics) {
    for (int i = 0; i < NOMBRE_CHOPSTICKS_PHILOSOPHERS; i++) {
      plates[i].draw(graphics);
      chopsticks[i].draw(graphics);
    }
  }

}