package philosopherProgramm;

import java.awt.Color;

public class Paint {
  private final int BLACK = -1;
  private final int RED = 0;
  private final int BLUE = 1;
  private final int GREEN = 2;
  private final int YELLOW = 3;
  private final int WHITE = 4;
  protected Color color;

  public void setColor(int colorId) {
    if(colorId == BLACK) {
      this.color = Color.black;
    } else if(colorId == RED) {      
      this.color = Color.red;
    } else if(colorId == BLUE) {      
      this.color = Color.blue;
    } else if(colorId == GREEN) {      
      this.color = Color.green;
    } else if(colorId == YELLOW) {      
      this.color = Color.yellow;
    } else if(colorId == WHITE) {      
      this.color = Color.white;
    }
  }
  
  public void setInitialColor(){
    setColor(BLACK);
  }
}