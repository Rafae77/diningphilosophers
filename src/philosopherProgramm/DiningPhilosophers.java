package philosopherProgramm;

public class DiningPhilosophers {
  public static void main(String args[]) {
    GraphicTableServices table = new GraphicTableServices();
    table.createPhilosophers();
    table.philopherStartDinning();
  }
}
