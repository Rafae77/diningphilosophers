package philosopherProgramm;

import java.awt.*;
import java.awt.event.*;

public class GraphicTable extends Frame  implements WindowListener {
  private static final int LONGUEUR_FENETRE = 200;
  private static final int HAUTEUR_FENETRE = 200;
  static protected final int NOMBRE_CHOPSTICKS_PHILOSOPHERS = 5;
  private Point center;
  protected GraphicPlate plates[];
  protected GraphicChopstick chopsticks[];
  protected boolean  chopsticksFlag[];

  public GraphicTable() {
    super();
    center = new Point(LONGUEUR_FENETRE / 2, HAUTEUR_FENETRE / 2);
    createChopsticks();
    createPlates();
    createWindow();
    createChopstickFlags();
  }
  
  

  private void createChopsticks() {
    int chopStickStartAdjustementY = 70;
    int chopStickEndtAdjustementY = 40;
    chopsticks = new GraphicChopstick[NOMBRE_CHOPSTICKS_PHILOSOPHERS];
    Point chopstickStartCoordinates;
    Point chopstickEndCoordinates;

    for (int i = 0; i < NOMBRE_CHOPSTICKS_PHILOSOPHERS; i++) {
      chopstickStartCoordinates = new Point(center.x, center.y - chopStickStartAdjustementY);
      chopstickEndCoordinates = new Point(center.x, center.y - chopStickEndtAdjustementY);
      chopsticks[i] = new GraphicChopstick(center, chopstickStartCoordinates, chopstickEndCoordinates);
    }
  }

  private void createPlates() {
    int chopStickAdjustementY = 70;
    plates = new GraphicPlate[NOMBRE_CHOPSTICKS_PHILOSOPHERS];
    Point plateCoordinates = new Point(center.x, center.y - chopStickAdjustementY);

    for (int i = 0; i < NOMBRE_CHOPSTICKS_PHILOSOPHERS; i++) {
      plates[i] = new GraphicPlate(center, plateCoordinates);
    }
  }

  private void createWindow() {
    super.addWindowListener(this);
    super.setTitle("Dining Philosophers");
    super.setSize(LONGUEUR_FENETRE, HAUTEUR_FENETRE);
    super.setBackground(Color.darkGray);
    super.show();
    super.setResizable(false);
  }

  private void createChopstickFlags() {
    chopsticksFlag = new boolean[NOMBRE_CHOPSTICKS_PHILOSOPHERS];
    for (int i = 0; i < NOMBRE_CHOPSTICKS_PHILOSOPHERS; i++) {
      chopsticksFlag[i] = true;
    }
  }
  
  public void windowOpened(WindowEvent evt) {
  }

  public void windowClosing(WindowEvent evt) {
    System.exit(0);
  }

  public void windowClosed(WindowEvent evt) {
  }

  public void windowIconified(WindowEvent evt) {
  }

  public void windowDeiconified(WindowEvent evt) {
  }

  public void windowActivated(WindowEvent evt) {
  }

  public void windowDeactivated(WindowEvent evt) {
  }
}
