package philosopherProgramm;

public class Philosopher extends Thread {
  final int TIME_THINK_MAX = 4000;
  final int TIME_NEXT_CHOPSTICK = 1000;
  final int TIME_EAT_MAX = 3000;
  final long TIME_BEFORE_START = 500l;

  private GraphicTableServices table;
  private int leftChopstick;
  private int rightChopstick;
  private int philosopherId;

  public Philosopher(int philosopherId, GraphicTableServices table, int leftChopstick, int rightChopstick) {
    this.philosopherId = philosopherId;
    this.table = table;
    this.leftChopstick = leftChopstick;
    this.rightChopstick = rightChopstick;
    super.setName("Philosopher " + philosopherId);
  }

  public void run() {
    this.doSleep(TIME_BEFORE_START);
    while (true) {
      philosopherThinks();
      philosopherBecomesHungry();
      philosopherTakesleftChopstick();
      this.doSleep(TIME_NEXT_CHOPSTICK);
      philosopherTakesRigthChopstick();
      philosopherEats();
      philosopherRealeaseleftChopstickChopstick();
      philosopherRealeaserightChopstickChopstick();
    }
  }

  private void doSleep(long millis) {
    try {
      sleep(millis);

    } catch (InterruptedException e) {
      System.out.println(e);
    }
  }

  public void philosopherThinks() {
    table.doThinking(philosopherId);
    System.out.println(super.getName() + " thinks");
    this.doSleep((long) (Math.random() * TIME_THINK_MAX));
    System.out.println(super.getName() + " finished thinking");
  }

  public void philosopherBecomesHungry() {
    System.out.println(super.getName() + " is hungry");
    table.becomesHungry(philosopherId);
  }

  public void philosopherTakesleftChopstick() {
    System.out.println(super.getName() + " wants leftChopstick chopstick");
    table.take(leftChopstick);
    table.takeChopstick(philosopherId, leftChopstick);
    System.out.println(super.getName() + " got leftChopstick chopstick");
  }

  public void philosopherTakesRigthChopstick() {
    System.out.println(super.getName() + " wants rightChopstick chopstick");
    table.take(rightChopstick);
    table.takeChopstick(philosopherId, rightChopstick);
    System.out.println(super.getName() + " got rightChopstick chopstick");
  }

  public void philosopherEats() {
    final double timeToSleep = Math.random() * TIME_EAT_MAX;
    System.out.println(super.getName() + " eats for " + timeToSleep);
    this.doSleep((long) timeToSleep);
    System.out.println(super.getName() + " finished eating");
  }

  public void philosopherRealeaserightChopstickChopstick() {
    table.releaseChopstick(rightChopstick);
    table.release(rightChopstick);
    System.out.println(super.getName() + " released rightChopstick chopstick");
  }

  public void philosopherRealeaseleftChopstickChopstick() {
    table.releaseChopstick(leftChopstick);
    table.release(leftChopstick);
    System.out.println(super.getName() + " released leftChopstick chopstick");
  }
}
