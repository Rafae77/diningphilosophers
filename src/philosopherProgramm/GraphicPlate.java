package philosopherProgramm;

import java.awt.*;

public class GraphicPlate extends Paint {
  private Point plateCoordinates;
  private int angle;
  private int degreeValueMutiply = 72;
  private int degreeCorrectionY = 5;
  private int degreeCorrectionX = 10;
  private final int plateSize = 20;
  static private int plateId = 0;

  public GraphicPlate(Point center, Point plateCoordinates) {
    super.setInitialColor();
    this.angle = degreeValueMutiply * plateId;
    this.plateCoordinates = new Point(movePlate(plateCoordinates.x, plateCoordinates.y, center.x, center.y, angle));
    this.plateCoordinates.x -= degreeCorrectionX;
    this.plateCoordinates.y += degreeCorrectionY;
    plateId++;
  }

  private Point movePlate(int coordinateX, int coordinateY, int centerX, int centerY, int angle) {
    Point nouvellePosition = new Point();
    double rotation = Math.PI / (180.0 / angle);

    nouvellePosition.y = (int) (coordinateX * Math.sin(rotation) + coordinateY * Math.cos(rotation)
        - Math.sin(rotation) * centerX - Math.cos(rotation) * centerY + centerY);
    nouvellePosition.x = (int) (coordinateX * Math.cos(rotation) - coordinateY * Math.sin(rotation)
        - Math.cos(rotation) * centerX + Math.sin(rotation) * centerY + centerX);
    return (nouvellePosition);
  }

  public void draw(Graphics g) {
    g.setColor(color);
    g.fillOval(plateCoordinates.x, plateCoordinates.y, plateSize, plateSize);
  }

  public Point getPlatePosition() {
    return this.plateCoordinates;
  }
}
