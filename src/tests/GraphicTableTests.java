package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import philosopherProgramm.GraphicTableServices;
import philosopherProgramm.Philosopher;

public class GraphicTableTests {
  final int TIME_DEADLOCK = 24000;
  GraphicTableServices table;
  Philosopher philosopher0;
  Philosopher philosopher1;
  Philosopher philosopher2;
  Philosopher philosopher3;
  Philosopher philosopher4;

  @Before
  public void setUp() throws Exception {
    table = new GraphicTableServices();
    philosopher0 = new Philosopher(0, table, 0, 4);
    philosopher1 = new Philosopher(1, table, 1, 0);
    philosopher2 = new Philosopher(2, table, 2, 1);
    philosopher3 = new Philosopher(3, table, 3, 2);
    philosopher4 = new Philosopher(4, table, 4, 3);
  }

  @After
  public void tearDown() throws Exception {
    table = null;
    philosopher0 = null;
    philosopher1 = null;
    philosopher2 = null;
    philosopher3 = null;
    philosopher4 = null;
  }

  @Test(timeout = TIME_DEADLOCK)
  public void deadlockCreation() throws Exception {
    philospherThinks();
    philosopherBecomesHungry();
    philosophersTakeAllChopsticks();
    philosopher0TryToTakeRigthChopstick();
  }

  private void philospherThinks() {
    philosopher0.philosopherThinks();
    philosopher1.philosopherThinks();
    philosopher2.philosopherThinks();
    philosopher3.philosopherThinks();
    philosopher4.philosopherThinks();
  }

  private void philosopherBecomesHungry() {
    philosopher0.philosopherBecomesHungry();
    philosopher1.philosopherBecomesHungry();
    philosopher2.philosopherBecomesHungry();
    philosopher3.philosopherBecomesHungry();
    philosopher4.philosopherBecomesHungry();
  }

  private void philosophersTakeAllChopsticks() {
    philosopher0.philosopherTakesleftChopstick();
    philosopher1.philosopherTakesleftChopstick();
    philosopher2.philosopherTakesleftChopstick();
    philosopher3.philosopherTakesleftChopstick();
    philosopher4.philosopherTakesleftChopstick();
  }

  private void philosopher0TryToTakeRigthChopstick() {
    philosopher0.philosopherTakesRigthChopstick();
  }
}
