package tests;

import static org.junit.Assert.*;

import java.awt.Point;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import philosopherProgramm.GraphicPlate;

public class GraphicPlateTest {

  GraphicPlate plates[];
  int LONGUEUR_FENETRE = 200;
  int HAUTEUR_FENETRE = 200;
  int NOMBRE_PLATES = 5;
  Point center;
  Point positionPlate1 = new Point(90, 35);
  Point positionPlate2 = new Point(156, 83);
  Point positionPlate3 = new Point(131, 161);
  Point positionPlate4 = new Point(48, 161);
  Point positionPlate5 = new Point(23, 83);

  @Before
  public void setUp() throws Exception {
    center = new Point(LONGUEUR_FENETRE / 2, HAUTEUR_FENETRE / 2);
    plates = new GraphicPlate[NOMBRE_PLATES];
    for (int i = 0; i < NOMBRE_PLATES; i++) {
      plates[i] = new GraphicPlate(center, new Point(center.x, center.y - 70));
    }
  }

  @After
  public void tearDown() throws Exception {
    plates = null;
    center = null;
  }

  @Test
  public void Plate1() {
    assertEquals(positionPlate1, plates[0].getPlatePosition());
  }

  @Test
  public void Plate2() {
    assertEquals(positionPlate2, plates[1].getPlatePosition());
  }

  @Test
  public void Plate3() {
    assertEquals(positionPlate3, plates[2].getPlatePosition());
  }

  @Test
  public void Plates4() {
    assertEquals(positionPlate4, plates[3].getPlatePosition());
  }

  @Test
  public void Plate5() {
    assertEquals(positionPlate5, plates[4].getPlatePosition());
  }
}
