package tests;

import static org.junit.Assert.*;

import java.awt.Point;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import philosopherProgramm.GraphicChopstick;

public class GraphicChopstickTests {
  GraphicChopstick chops[];
  int LONGUEUR_FENETRE = 200;
  int HAUTEUR_FENETRE = 200;
  int NOMBRE_CHOPSTICKS = 5;
  Point center;
  Point ChopStick1StartCoordinates = new Point(141, 58);
  Point ChopStick1EndCoordinates = new Point(123, 82);
  Point ChopStick2StartCoordinates = new Point(166, 136);
  Point ChopStick2EndCoordinates = new Point(138, 127);
  Point ChopStick3StartCoordinates = new Point(100, 185);
  Point ChopStick3EndCoordinates = new Point(100, 155);
  Point ChopStick4StartCoordinates = new Point(33, 136);
  Point ChopStick4EndCoordinates = new Point(61, 127);
  Point ChopStick5StartCoordinates = new Point(58, 58);
  Point ChopStick5EndCoordinates = new Point(76, 82);

  @Before
  public void setUp() throws Exception {
    center = new Point(LONGUEUR_FENETRE / 2, HAUTEUR_FENETRE / 2);
    chops = new GraphicChopstick[NOMBRE_CHOPSTICKS];
    for (int i = 0; i < NOMBRE_CHOPSTICKS; i++) {
      chops[i] = new GraphicChopstick(center, new Point(center.x, center.y - 70), new Point(center.x, center.y - 40));
    }
  }

  @After
  public void tearDown() throws Exception {
    chops = null;
    center = null;
  }

  @Test
  public void testChopstick1Start_OriginalCoordinates() {
    assertEquals(ChopStick1StartCoordinates, chops[0].getcoordinatesStart());
  }

  @Test
  public void testChopstick1End_OriginalCoordinates() {
    assertEquals(ChopStick1EndCoordinates, chops[0].getcoordinatesEnd());
  }

  @Test
  public void testChopstick2Start_OriginalCoordinates() {
    assertEquals(ChopStick2StartCoordinates, chops[1].getcoordinatesStart());
  }

  @Test
  public void testChopstick2End_OriginalCoordinates() {
    assertEquals(ChopStick2EndCoordinates, chops[1].getcoordinatesEnd());
  }

  @Test
  public void testChopstick3Start_OriginalCoordinates() {
    assertEquals(ChopStick3StartCoordinates, chops[2].getcoordinatesStart());
  }

  @Test
  public void testChopstick3End_OriginalCoordinates() {
    assertEquals(ChopStick3EndCoordinates, chops[2].getcoordinatesEnd());
  }

  @Test
  public void testChopstick4Start_OriginalCoordinates() {
    assertEquals(ChopStick4StartCoordinates, chops[3].getcoordinatesStart());
  }

  @Test
  public void testChopstick4End_OriginalCoordinates() {
    assertEquals(ChopStick4EndCoordinates, chops[3].getcoordinatesEnd());
  }

  @Test
  public void testChopstick5Start_OriginalCoordinates() {
    assertEquals(ChopStick5StartCoordinates, chops[4].getcoordinatesStart());
  }

  @Test
  public void testChopstick5End_OriginalCoordinates() {
    assertEquals(ChopStick5EndCoordinates, chops[4].getcoordinatesEnd());
  }
}